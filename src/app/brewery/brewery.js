(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.brewery', [
    'cz.angular.examples.beerApp.brewery.edit',
    'cz.angular.examples.beerApp.brewery.services'
  ])
      .config(function($stateProvider) {
        $stateProvider
            .state('brewery', {
              url: '/brewery',
              template: '<ui-view/>',
              abstract: true,
              data: {
                authRoles: ['ROLE_ADMIN']
              }
            })

            .state('brewery.create', {
              url: '/create',
              templateUrl: 'app/brewery/edit/edit.html',
              controller: 'brewery.EditController',
              controllerAs: 'edit',
              resolve: {
                brewery: function() {
                  return {};
                }
              },
              data: {
                title: 'Přidat pivovar',
                displayName: 'Přidat pivovar'
              }
            })

            .state('brewery.edit', {
              url: '/edit/:id',
              templateUrl: 'app/brewery/edit/edit.html',
              controller: 'brewery.EditController',
              controllerAs: 'edit',
              resolve: {
                brewery: function(breweryService, $stateParams) {
                  return breweryService.get($stateParams.id);
                }
              },
              data: {
                title: 'Upravit pivovar',
                displayName: 'Upravit pivovar'
              }
            });
      });

})();