(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.brewery.services', [
    'ngResource'
  ])
      .factory('BreweryResourceFactory', function($resource, API_URI) {
        return $resource(API_URI + '/brewery/:id',
            {
              'id': '@id'
            },
            {
              'update': {method: 'PUT'}
            }
        );
      })

      .service('breweryService', function($q, BreweryResourceFactory, filterFilter) {
        this.breweries = null;
        this.breweriesPromise = null;

        /**
         * @returns {Promise}
         */
        this.load = function() {
          if (this.breweries) {
            return $q.when(this.breweries);
          }

          if (this.breweriesPromise) {
            return this.breweriesPromise;
          }

          this.breweriesPromise = BreweryResourceFactory.query().$promise
              .then(function(breweries) {
                this.breweries = breweries;
                return this.breweries;
              }.bind(this));

          return this.breweriesPromise;
        };

        /**
         * @param breweryData
         * @returns {BreweryResourceFactory}
         */
        this.create = function(breweryData) {
          return new BreweryResourceFactory(breweryData);
        };

        /**
         * @param brewery
         * @returns {Promise}
         */
        this.add = function(brewery) {
          return BreweryResourceFactory.save(brewery).$promise
              .then(function(brewery) {
                var currentObject = this.get(brewery.id);
                if (currentObject) {
                  angular.extend(brewery, currentObject);
                } else {
                  this.breweries.push(brewery);
                }
                return brewery;
              }.bind(this));
        };

        /**
         * @param id
         * @returns {Promise}
         */
        this.get = function(id) {
          return this.load().then(function(breweries) {
            var matchedItems = filterFilter(breweries, {id: parseInt(id)}, true);
            if (matchedItems.length === 1) {
              return matchedItems.shift();
            } else {
              // TODO ERROR?
              return;
            }
          });
        };

        this.load();
      });

})();