(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp', [
    'cz.angular.examples.beerApp.templates',
    'ngMessages',
    'ui.router',
    'ct.ui.router.extras',
    'angularUtils.directives.uiBreadcrumbs',
    'ngProgress',

    'cz.angular.auth.adapter.uiRouter',
    'cz.angular.uiBindData',
    'cz.angular.bootstrapNotifications',

    'cz.angular.examples.beerApp.beer',
    'cz.angular.examples.beerApp.brewery',
    'cz.angular.examples.beerApp.errors',
    'devel.auth',
    'angularStats'
  ])

      .constant('API_URI', 'api')

      .config(function(authServiceSettingsProvider, API_URI) {
        authServiceSettingsProvider.setLoginUrl(API_URI + '/login');
        authServiceSettingsProvider.setTokenRenewUrl(API_URI + '/renew');
      })

      .config(function($urlRouterProvider) {
        $urlRouterProvider
            .otherwise('/beer');
      })

      .config(function(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
          v: '3.22',
          libraries: 'geometry'
        });
      })

      .run(function($rootScope, $state, $log, ngProgressFactory) {
        var ngProgress = ngProgressFactory.createInstance();
        ngProgress.setColor('SkyBlue');

        function error403(event) {
          $log.log(arguments);
          $state.go('error.403');
        }



        $rootScope.$on('cz.angular.auth:permissionError', error403);

        function error404(event) {
          $log.log(arguments);
          $state.go('error.404');
        }

        $rootScope.$on('$stateNotFound', error404);

        function error500(event) {
          $log.log(arguments);
          $state.go('error.500');
        }

        $rootScope.$on('$stateChangeError', error500);

        $rootScope.$on('$stateChangeStart', function() {
          ngProgress.start();
        });

        $rootScope.$on('$stateChangeSuccess', function() {
          ngProgress.complete();
        });


      });

})();