describe('application', function() {

  beforeEach(module('cz.angular.examples.beerApp'));

  it('should have defined module', function() {
    expect(angular.module('cz.angular.examples.beerApp')).toBeDefined();
  });
});