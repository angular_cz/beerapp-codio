(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.beer', [
    'cz.angular.examples.beerApp.beer.list',
    'cz.angular.examples.beerApp.beer.detail',
    'cz.angular.examples.beerApp.beer.edit',
    'cz.angular.examples.beerApp.beer.services',

    'cz.angular.examples.beerApp.common.brewery.brewerySelect',

    'ngResource',
    'ui.bootstrap'
  ])

      .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('beers', {
              url: '/beer',
              template: '<ui-view></ui-view>',
              abstract: true,
              data: {
                breadcrumbProxy: 'beers.list'
              }
            })

            .state('beers.list', {
              url: '',
              templateUrl: 'app/beer/list/list.html',
              controller: 'beer.ListController',
              controllerAs: 'list',
              resolve: {
                listData: function(beerService, beerSearchParams, $location) {
                  var search = $location.search();
                  return beerSearchParams.applyFromSearch(search)
                    .then(function(searchParams) {
                      return beerService.query(searchParams);
                    });
                }
              },
              data: {
                displayName: 'Seznam piv'
              }
            })

            .state('beers.create', {
              url: '/create',
              templateUrl: 'app/beer/edit/edit.html',
              controller: 'beer.EditController',
              controllerAs: 'edit',
              resolve: {
                beer: function(beerService) {
                  return beerService.newBeer();
                }
              },
              data: {
                displayName: 'Přidat pivo',
                title: 'Přidat pivo',
                authRoles: ['ROLE_ADMIN']
              }
            })

            .state('beers.beer', {
              url: '/:id?',
              abstract: true,
              resolve: {
                beer: function(beerService, $stateParams) {
                  return beerService.getBeer($stateParams.id);
                }

              },
              views: {
                '': {
                  templateUrl: 'app/beer/beer.html'
                },
                'sidebar@beers.beer': {
                  controller: 'beer.SidebarController',
                  controllerAs: 'sidebarController',
                  templateUrl: 'app/beer/detail/detail.sidebar.html'
                }
              }
            })

            .state('beers.beer.detail', {
              url: '',
              resolve: {
                brewery: function(beer) {
                  return beer.brewery;
                },
                mapApiReady: function(uiGmapGoogleMapApi) {
                  return uiGmapGoogleMapApi;
                }
              },
              views: {
                'main': {
                  controller: 'beer.DetailController',
                  controllerAs: 'detail',
                  templateUrl: 'app/beer/detail/detail.html'
                },
                'sidebarContent@beers.beer': {
                  controller: 'beer.BreweryController',
                  controllerAs: 'breweryController',
                  templateUrl: 'app/beer/detail/brewery/brewery.html'
                }
              },
              data: {
                displayName: '{{beer.name}} ({{beer.brewery.name}})'
              }
            })

            .state('beers.beer.detail.rating', {
              url: '/rating',
              views: {
                'sidebarContent@beers.beer': {
                  controller: 'beer.RatingController',
                  controllerAs: 'ratingController',
                  templateUrl: 'app/beer/detail/rating/rating.html'
                }
              },
              data: {
                displayName: 'hodnocení'
              }
            })

            .state('beers.beer.detail.edit', {
              url: '/edit',
              views: {
                'main@beers.beer': {
                  templateUrl: 'app/beer/edit/edit.html',
                  controller: 'beer.EditController',
                  controllerAs: 'edit'
                },
                'sidebar@beers.beer': {
                  template: ''
                }
              },
              data: {
                displayName: 'editace',
                title: 'Upravit pivo',
                authRoles: ['ROLE_ADMIN']
              }
            });

      });

})();