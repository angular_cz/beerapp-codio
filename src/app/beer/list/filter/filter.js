(function() {
  'use strict';

  var FilterInRowController = function($scope) {

    this.$onChanges = function(changeObject) {
      this.filter = angular.copy(this.outerFilter);
    };

    this.clearFilter = function() {
      this.onReset({filter: this.filter});
    };

    this.updateListener_ = function(newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      }

      this.onChange({filter: this.filter});
    };

    $scope.$watch('filterController.filter', this.updateListener_.bind(this), true);
  };

  angular.module('cz.angular.examples.beerApp.beer.list.filter', [
    'cz.angular.examples.beerApp.common.beer.degreeSelect',
    'cz.angular.examples.beerApp.beer.services'
  ])
    .directive('filterInRow', function() {
      return {
        templateUrl: "app/beer/list/filter/filter.html",
        bindToController: {
          outerFilter: "<filterInRow",
          onChange: "&",
          onReset: "&"
        },
        controller: FilterInRowController,
        controllerAs: "filterController"
      };

    });

})();
