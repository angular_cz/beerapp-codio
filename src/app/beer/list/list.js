(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.beer.list', [
    'cz.angular.examples.beerApp.beer.list.filter',
    'cz.angular.bootstrapNotifications'
  ])
      .controller('beer.ListController', function($scope, listData, beerService, beerSearchParams, notifications, $location) {

        this.pagingInfo = {};
        this.beerSearchParams = beerSearchParams;

        $scope.$on('$locationChangeSuccess', function() {
          this.beerSearchParams.applyFromSearch($location.search())
            .then(beerService.query.bind(beerService))
            .then(this.actualizeBeers.bind(this));
        }.bind(this));

        this.remove = function(beer) {
          beer.$delete()
              .then(function() {
                notifications.addSuccess('Pivo bylo smazáno.');
                this.reload();
              }.bind(this))
              .catch(function() {
                notifications.addError('Došlo k chybě při mazání piva.');
              });
        };

        this.reload = function() {
          $location.search(this.beerSearchParams.getSearchParams());
        };

        this.search = function(filter) {
          this.beerSearchParams.applyFilter(filter);
          this.reload();
        };

        this.reset = function() {
          this.beerSearchParams.reset();
          this.reload();
        };

        this.actualizeBeers = function(data) {
          this.beers = data.beers;
          this.pagingInfo = angular.extend(this.pagingInfo, data.paging);
        };

        this.actualizeBeers(listData);
      });

})();