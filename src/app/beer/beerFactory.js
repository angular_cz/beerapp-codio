(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.beer.services', [])
      .factory('BeerResourceFactory', function($resource, API_URI) {
        return $resource(API_URI + '/beer/:id',
            {'id': '@id'},
            {
              'update': {method: 'PUT'},
              'query': {
                method: 'GET', isArray: true, transformResponse: function(data, headersGetter) {
                  return JSON.parse(data).content;
                }
              }
            });
      })

      .service('beerSearchParams', function($q, breweryService) {

        this.filter = {};

        this.applyFromSearch = function(locationSearch) {
          this.reset();
          this.filter.query = locationSearch.query;
          this.filter.degree = locationSearch.degree;
          this.filter.alcohol = parseFloat(locationSearch.alcohol) || null;
          this.filter.alcoholOperator = locationSearch.alcoholOperator || this.filter.alcoholOperator;
          this.page = parseInt(locationSearch.page) || 1;

          if(locationSearch.brewery) {
            return breweryService.get(locationSearch.brewery)
              .then(function(brewery) {
                this.filter.brewery = brewery;
                return this;
            }.bind(this));
          }

          return $q.when(this);
        };

        this.applyFilter = function(filter) {
          this.filter = angular.copy(filter);
          this.page = 1;
        };

        this.reset = function() {
          this.page = 1;
          this.filter = {};
          this.filter.alcoholOperator = '=';
        };

        this.getSearchParams = function() {
          var params = {
            page: this.page
          };

          if (this.filter.query) {
            params.query = this.filter.query;
          }

          if (this.filter.degree) {
            params.degree = this.filter.degree;
          }

          if (this.filter.brewery) {
            params.brewery = this.filter.brewery.id;
          }

          if (this.filter.alcohol) {
            params.alcohol = this.filter.alcohol;
            params.alcoholOperator = this.filter.alcoholOperator;
          }

          return params;
        };

        this.reset();

      })

      .service('beerService', function($http, BeerResourceFactory, API_URI) {
        this.newBeer = function() {
          return new BeerResourceFactory();
        };

        this.query = function(filter) {
          return $http.get(API_URI + '/beer', {params: filter.getSearchParams()})
              .then(function(response) {

                var transformedList = response.data.content.map(function(element) {
                  return new BeerResourceFactory(element);
                });

                return {
                  beers: transformedList,
                  paging: {
                    totalItems: response.data.totalElements,
                    perPage: response.data.size
                  }
                };
              });
        };

        this.getBeer = function(id) {
          return BeerResourceFactory.get({id: id}).$promise;
        };
      })

      .service('ratingService', function($http, API_URI) {

        this.add = function(beer, rating) {
          return $http.post(API_URI + '/beer/' + beer.id + '/ratings', rating);
        };
      });

})();