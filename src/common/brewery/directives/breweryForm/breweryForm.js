(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.common.brewery.breweryForm', [
        'ui.select',
        'cz.angular.examples.beerApp.common.brewery.brewerySelect',
        'cz.angular.examples.beerApp.brewery.services'
      ]
  )
      .directive('breweryForm', function() {

        var BreweryFormController = function(breweryService) {
          this.save = function() {
            if (this.form.$invalid) {
              return;
            }

            breweryService.add(this.brewery)
                .then(function(brewery) {
                  this.onSave({brewery: brewery});
                }.bind(this),
                function(error) {
                  this.onError({error: error});
                }.bind(this)
            );
          };

          this.cancel = function() {
            this.onCancel();
          };
        };

        return {
          restrict: 'E',
          templateUrl: 'common/brewery/directives/breweryForm/breweryForm.html',
          scope: {
            title: '@',
            brewery: '=',
            onSave: '&',
            onCancel: '&',
            onError: '&'
          },
          bindToController: true,
          controllerAs: 'edit',
          controller: BreweryFormController
        };
      })

      .config(function(uiSelectConfig) {
        uiSelectConfig.theme = 'bootstrap';
      });

})();