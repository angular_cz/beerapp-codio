(function() {
  'use strict';

  function NotificationBuilder(notifications, title, text) {

    this.notifications = notifications;

    this.data = {
      title: title,
      text: text
    };
  }

  NotificationBuilder.prototype.show = function() {
    return this.notifications.add_(this.data);
  };

  NotificationBuilder.prototype.info = function() {
    this.data.type = this.notifications.types.info;
    return this;
  };

  NotificationBuilder.prototype.success = function() {
    this.data.type = this.notifications.types.success;
    return this;
  };

  NotificationBuilder.prototype.warning = function() {
    this.data.type = this.notifications.types.warning;
    return this;
  };

  NotificationBuilder.prototype.error = function() {
    this.data.type = this.notifications.types.error;
    return this;
  };

  NotificationBuilder.prototype.timeout = function(timeoutInMiliSeconds) {
    this.data.timeout = timeoutInMiliSeconds;
    return this;
  };

  NotificationBuilder.prototype.persistent = function() {
    this.data.timeout = undefined;
    return this;
  };

  NotificationBuilder.prototype.button = function(buttonText) {
    this.data.buttonText = buttonText;
    return this;
  };

  var NotificationService = function($q, $timeout, types, settings) {

    this.notifications = [];

    this.types = types;
    this.settings = settings;

    /**
     * @param title
     * @param text
     * @returns {NotificationBuilder}
     */
    this.build = function(title, text) {
      return new NotificationBuilder(this, title, text);
    };

    /**
     * @param title
     * @param text
     * @returns {*}
     */
    this.addInfo = function(title, text) {
      return this.build(title, text).info().show();
    };

    /**
     * @param title
     * @param text
     * @returns {*}
     */
    this.addSuccess = function(title, text) {
      return this.build(title, text).success().show();
    };

    /**
     * @param title
     * @param text
     * @returns {*}
     */
    this.addWarning = function(title, text) {
      return this.build(title, text).warning().show();
    };

    /**
     * @param title
     * @param text
     * @returns {*}
     */
    this.addError = function(title, text) {
      return this.build(title, text).error().show();
    };

    this.getDefaultData_ = function(type) {
      return {
        type: type || this.settings.defaultType
      };
    };

    /**
     * @param notificationData
     * @returns {*}
     * @private
     */
    this.add_ = function(notificationData) {
      var defaultData = this.getDefaultData_(notificationData.type);

      var notificationObject = angular.extend(defaultData, notificationData);
      this.notifications.unshift(notificationObject);

      if (this.settings.persistentTypes.indexOf(notificationObject.type) === -1 &&
          !notificationObject.hasOwnProperty('timeout')
      ) {
        if (notificationObject.hasOwnProperty('buttonText') && this.settings.timeouts.hasOwnProperty('button')) {
          notificationObject.timeout = this.settings.timeouts.button;
        } else {
          notificationObject.timeout = this.settings.timeouts.default;
        }
      }

      if (notificationObject.hasOwnProperty('timeout') && angular.isNumber(notificationObject.timeout)) {
        $timeout(this.remove.bind(this, notificationObject), notificationObject.timeout);
      }

      return $q(function(resolve, reject) {
        notificationObject.onHide = function(reason) {
          if (notificationObject.hasOwnProperty('buttonText')) {
            reject(reason);
          } else {
            resolve(notificationObject);
          }
        };

        if (notificationObject.hasOwnProperty('buttonText')) {
          notificationObject.onClick = function() {
            resolve(notificationObject);
          };
        }
      });
    };

    /**
     * @param notificationObject
     */
    this.remove = function(notificationObject) {
      if (this.remove_(notificationObject)) {
        notificationObject.onHide('remove');
      }
    };

    /**
     * @param notificationObject
     * @returns {boolean}
     * @private
     */
    this.remove_ = function(notificationObject) {
      var index = this.notifications.indexOf(notificationObject);
      if (index >= 0) {
        this.notifications.splice(index, 1);
        return true;
      }

      return false;
    };

    /**
     * @param notificationObject
     */
    this.click_ = function(notificationObject) {
      notificationObject.onClick();
      this.remove_(notificationObject);
    };
  };

  angular.module('cz.angular.notifications.service', [])
      .provider("notifications", function() {
        this.types = {
          success: "success",
          info: "info",
          warning: "warning",
          danger: "danger",
          error: "danger"
        };

        this.settings = {
          defaultType: this.types.info,
          timeouts: {
            default: 5000
          },
          persistentTypes: [
            this.types.error
          ]
        };

        /**
         * @param type
         */
        this.setDefaultType = function(type) {
          this.settings.defaultType = type;
        };

        /**
         * @param types array
         */
        this.setPersistentTypes = function(types) {
          if (angular.isArray(types)) {
            this.settings.persistentTypes = types;
          } else {
            this.settings.persistentTypes = Array.prototype.slice.call(arguments);
          }
        };

        /**
         * @param timeout Number
         */
        this.setTimeout = function(timeout) {
          this.settings.timeouts.default = timeout;
        };

        /**
         * @param timeout Number
         */
        this.setButtonTimeout = function(timeout) {
          this.settings.timeouts.button = timeout;
        };

        this.$get = function($q, $timeout) {
          return new NotificationService($q, $timeout, this.types, this.settings);
        };
      });

})();