(function() {
  'use strict';

  angular.module('cz.angular.bootstrapNotifications', [
    'ui.bootstrap.alert',
    'cz.angular.notifications.service'
  ])
      .directive("notifications", function() {
        return {
          templateUrl: 'common/cz/angular/notifications/bootstrap-notifications.html',
          controllerAs: 'notificationsController',
          controller: function(notifications) {
            this.notificationsService = notifications;

            this.close = function(notification) {
              this.notificationsService.remove(notification);
            };

            this.buttonClick = function(notification) {
              this.notificationsService.click_(notification);
            };
          }

        };
      });

})();