describe('cz.angular.notifications.service', function() {

  beforeEach(module('cz.angular.notifications.service'));

  it("should have defined module", function() {
    expect(angular.module('cz.angular.notifications.service')).toBeDefined();
  });

  describe('notificationService', function() {
    var notificationsService;
    beforeEach(inject(function(notifications) {
      notificationsService = notifications;
    }));

    it('should have no notifications', function() {
      expect(notificationsService.notifications.length).toBe(0);
    });

    it('should have method for simple info adding', function() {
      notificationsService.addInfo('title', 'text');

      expect(notificationsService.notifications.length).toBe(1);
      expect(notificationsService.notifications[0].type).toBe(notificationsService.types.info);
    });

    it('should have method for simple success adding', function() {
      notificationsService.addSuccess('title', 'text');
      expect(notificationsService.notifications[0].type).toBe(notificationsService.types.success);
    });

    it('should have method for simple add warning', function() {
      notificationsService.addWarning('title', 'text');
      expect(notificationsService.notifications[0].type).toBe(notificationsService.types.warning);
    });

    it('should have method for simple add error', function() {
      notificationsService.addError('title', 'text');
      expect(notificationsService.notifications[0].type).toBe(notificationsService.types.error);
    });

    it('should provide interface for notification build', function() {
      var notification = notificationsService.build('title', 'text');
      expect(notificationsService.notifications.length).toBe(0);

      notification.show();
      expect(notificationsService.notifications.length).toBe(1);
    });

    it('should add notification at the beginning notifications', function() {
      notificationsService.addInfo('first', 'text');
      notificationsService.addInfo('second', 'text');

      expect(notificationsService.notifications[0].title).toBe('second');
    });

    describe('builder', function() {

      it('should have fluent methods for type modification', function() {
        var notification = notificationsService.build('title', 'text');
        notification.info();

        expect(notification.data.type).toBe(notificationsService.types.info);

        notification.info().success();
        expect(notification.data.type).toBe(notificationsService.types.success);

        notification.warning();
        expect(notification.data.type).toBe(notificationsService.types.warning);

        notification.error();
        expect(notification.data.type).toBe(notificationsService.types.error);
      });

      it('should have fluent methods timeout settings', function() {
        var notification = notificationsService.build('title', 'text');
        notification.timeout(5000);
        expect(notification.data.timeout).toBe(5000);

        notification.timeout(5000).timeout(3000);
        expect(notification.data.timeout).toBe(3000);
      });

      it('should have fluent method for timeout unset', function() {
        var notification = notificationsService.build('title', 'text');
        notification.timeout(5000).persistent();
        expect(notification.data.timeout).not.toBeDefined();

        expect(notification.persistent()).toBe(notification);
      });

      it('should have fluent method button for user button definition', function() {
        var notification = notificationsService.build('title', 'text');
        notification.button('button text');

        expect(notification.data.buttonText).toBe('button text');
        expect(notification.button('button text')).toBe(notification);
      });

      it('should show info if type is not defined', function() {
        notificationsService.build('title', 'text').show();

        expect(notificationsService.notifications[0].type).toBe(notificationsService.types.info);
      });

    });

    describe('promise', function() {

      var rootScope;

      beforeEach(inject(function($rootScope) {
        rootScope = $rootScope;
      }));

      it('should be a return value', function() {
        var returnValue = notificationsService.addInfo('first', 'text');

        expect(returnValue.then).toBeDefined();
      });

      it('should be return after builder show call', function() {
        var builder = notificationsService.build('first', 'text');
        var returnValue = builder.show();

        expect(returnValue.then).toBeDefined();
      });

      it('should be resolved after remove call', function(done) {

        var calledFlag = false;
        var notificationPromise = notificationsService.addInfo('first', 'text');
        var notificationObject = notificationsService.notifications[0];

        notificationPromise.then(function(notificationObject) {
          calledFlag = true;
          expect(notificationObject).toBeDefined();
        });

        expect(calledFlag).toBe(false);
        notificationsService.remove(notificationObject);

        rootScope.$apply();
        expect(calledFlag).toBe(true);
        done();

      });

      it('should be rejected after remove, if button is defined', function(done) {

        var calledFlag;
        var notificationPromise = notificationsService.build('first', 'text').button('button').show();
        var notificationObject = notificationsService.notifications[0];

        notificationPromise
            .then(function(notificationObject) {
              calledFlag = 'resolve';
              expect(notificationObject).toBeDefined();
            },
            function(rejectReason) {
              calledFlag = 'reject';
              expect(rejectReason).toBe('remove');
            });

        expect(calledFlag).toBeUndefined();
        notificationsService.remove(notificationObject);

        rootScope.$apply();
        expect(calledFlag).toBe('reject');
        done();

      });

      it('should be resolved after buttonClick, if button is defined', function(done) {

        var calledFlag;
        var notificationPromise = notificationsService.build('first', 'text').button('button').show();
        var notificationObject = notificationsService.notifications[0];

        notificationPromise
            .then(function(notificationObject) {
              calledFlag = 'resolve';
              expect(notificationObject).toBeDefined();
            },
            function(rejectReason) {
              calledFlag = 'reject';
              expect(rejectReason).toBe('remove');
            });

        expect(calledFlag).toBeUndefined();
        notificationsService.click_(notificationObject);

        rootScope.$apply();
        expect(calledFlag).toBe('resolve');
        done();

      });

      it('should be removed after buttonClick, if button is defined', function() {

        notificationsService.build('first', 'text').button('button').show();

        expect(notificationsService.notifications.length).toBe(1);

        notificationsService.click_(notificationsService.notifications[0]);

        expect(notificationsService.notifications.length).toBe(0);
      });

    });

    describe('timeout', function() {
      it('should cause automatic removal', inject(function($timeout) {

        var notification = notificationsService.build('title', 'text');
        notification.timeout(5000).show();

        expect(notificationsService.notifications.length).toBe(1);

        $timeout.flush(5000);
        expect(notificationsService.notifications.length).toBe(0);
      }));

      it('should fulfilled promise', inject(function($timeout) {
        var callFlag = false;

        var notification = notificationsService.build('title', 'text');
        notification.timeout(5000).show()
            .then(function() {
              callFlag = true;
            });

        expect(callFlag).toBe(false);
        $timeout.flush(5000);
        expect(callFlag).toBe(true);
      }));

      it('should reject promise if contain button', inject(function($timeout) {
        var callFlag;

        var notification = notificationsService.build('title', 'text');
        notification.timeout(5000).button('revert').show()
            .then(function() {
              callFlag = 'resolve';
            },
            function() {
              callFlag = 'reject';
            });

        expect(callFlag).not.toBeDefined();
        $timeout.flush(5000);
        expect(callFlag).toBe('reject');
      }));
    });

  });

  describe('notificationsProvider', function() {

    beforeEach(
        module(function(notificationsProvider) {
          notificationsProvider.setDefaultType(notificationsProvider.types.warning);
          notificationsProvider.setPersistentTypes(notificationsProvider.types.warning, notificationsProvider.types.error);
          notificationsProvider.setTimeout(6000);
          notificationsProvider.setButtonTimeout(8000);
        })
    );

    it('should use default type by provider', inject(function(notifications) {
      notifications.build('title', 'text').show();
      expect(notifications.notifications[0].type).toBe(notifications.types.warning);
    }));

    it('should accept persistent types settings', inject(function(notifications) {
      notifications.build('title', 'text').warning().show();
      notifications.build('title', 'text').info().show();

      expect(notifications.notifications[1].timeout).not.toBeDefined();
      expect(notifications.notifications[0].timeout).toBeDefined();
    }));

    it('should apply specified timeout', inject(function(notifications, $timeout) {
      notifications.build('title', 'text').info().show();

      expect(notifications.notifications.length).toBe(1);

      $timeout.flush(5000);
      expect(notifications.notifications.length).toBe(1);
      $timeout.flush(1000);
      expect(notifications.notifications.length).toBe(0);
    }));

    it('should use special timeout for notification with button', inject(function(notifications, $timeout) {
      notifications.build('title', 'text').info().show();
      notifications.build('title 2', 'text').info().button('some button text').show();

      expect(notifications.notifications.length).toBe(2);

      $timeout.flush(6000);
      expect(notifications.notifications.length).toBe(1);
      expect(notifications.notifications[0].title).toBe('title 2');
      $timeout.flush(2000);
      expect(notifications.notifications.length).toBe(0);
    }));


    it('should permit set item as persistent', inject(function(notifications, $timeout) {
      notifications.build('title', 'text').info().show();
      notifications.build('title 2', 'text').info().persistent().show();

      expect(notifications.notifications.length).toBe(2);

      $timeout.flush(6000);
      expect(notifications.notifications.length).toBe(1);
      expect(notifications.notifications[0].title).toBe('title 2');
    }));


  });
});