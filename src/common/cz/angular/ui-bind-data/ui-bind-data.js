(function() {
  'use strict';

  angular.module('cz.angular.uiBindData', [
    'ui.router'
  ])
      .directive('uiBindData', function($compile, $state) {
        return {
          restrict: 'AC',
          compile: function uiDataBindCompile(templateElement) {
            $compile.$$addBindingClass(templateElement);

            return function uiDataBind(scope, element, attr) {
              $compile.$$addBindingInfo(element, attr.uiBindData);
              element = element[0];

              scope.$watch(function() {
                    return $state.current.data[attr.uiBindData];
                  },
                  function uiDataBindWatchAction(value) {
                    element.textContent = value === undefined ? '' : value;
                  });
            };

          }
        };
      });

})();