describe('cz.angular.auth.service', function() {

  beforeEach(module('cz.angular.auth.service'));

  it('should have defined module', function() {
    expect(angular.module('cz.angular.auth')).toBeDefined();
  });

  describe('authService', function() {

    describe('authentications', function() {

      it('should indicate unauthenticated user if info.user does not exists', inject(function(authService) {
        expect(authService.isAuthenticated()).toBe(false);
      }));

      it('should indicate authenticated user if info.user exists', inject(function(authService) {
        authService.info.user = {
          login: 'some user'
        };

        expect(authService.isAuthenticated()).toBe(true);
      }));
    });

    describe('roles', function() {

      it('should be possible to check', inject(function(authService) {
        var roles = ['ROLE_1', 'ROLE_3'];

        authService.info.user = {
          login: 'some user',
          roles: roles
        };

        expect(authService.hasRole('ROLE_1')).toBe(true);
        expect(authService.hasRole('ROLE_2')).toBe(false);
        expect(authService.hasRole('ROLE_3')).toBe(true);

      }));

      it('should be possible to check as array', inject(function(authService) {
        var roles = ['ROLE_1', 'ROLE_3'];

        authService.info.user = {
          login: 'some user',
          roles: roles
        };

        expect(authService.hasSomeRole(['ROLE_0', 'ROLE_1'])).toBe(true);
        expect(authService.hasRole(['ROLE_0', 'ROLE_2'])).toBe(false);
      }));

    });
  });

});