(function() {
  'use strict';

  var AuthServiceSettingsProvider = function() {
    var setting = {
      urls: {
        login: 'login',
        tokenRenew: 'renew'
      },
      tokenHeaderName: 'X-Auth-Token'
    };

    /**
     * @param url
     */
    this.setLoginUrl = function(url) {
      setting.urls.login = url;
    };

    /**
     * @param url
     */
    this.setTokenRenewUrl = function(url) {
      setting.urls.tokenRenew = url;
    };

    this.$get = function() {
      return setting;
    };
  };

  var authServiceFactory = function($http, $rootScope, $q, authTokenStore, authServiceSettings) {

    var successLogin = function(response) {
      auth.setToken(response.headers(authServiceSettings.tokenHeaderName));

      $rootScope.$broadcast('cz.angular.auth:loginSuccess');
      return response;
    };

    var failedLogin = function(response) {
      $rootScope.$broadcast('cz.angular.auth:loginFailed');
      return $q.reject(response);
    };

    var renewToken = function(response) {
      auth.setToken(response.headers(authServiceSettings.tokenHeaderName));
    };

    var auth = {
      info: authTokenStore.getInfo(),

      login: function(name, pass) {
        return $http.post(authServiceSettings.urls.login, {username: name, password: pass})
            .then(successLogin, failedLogin);
      },

      logout: function() {
        this.clearAuth();

        $rootScope.$broadcast('cz.angular.auth:loggedOut');
      },

      renewToken: function() {
        return $http.post(authServiceSettings.urls.tokenRenew, {})
            .then(renewToken);
      },

      setToken: function(token) {
        if (token === null) {
          this.clearAuth();
          return;
        }

        authTokenStore.setToken(token);
      },

      clearAuth: function() {
        authTokenStore.clear();
      },

      isAuthenticated: function() {
        return Boolean(this.info.user) || false;
      },

      hasRole: function(role) {
        if (!this.info.user) {
          return false;
        }

        if (!this.info.user.roles) {
          return false;
        }

        return this.info.user.roles.indexOf(role) !== -1;
      },

      hasSomeRole: function(roles) {
        if (!this.isAuthenticated()) {
          return false;
        }

        return roles.some(this.hasRole.bind(this));
      }
    };

    return auth;

  };

  var AuthTokenStore = function(jwtHelper, webStorage, $rootScope) {

    var TOKEN_STORE_KEY = 'cz.angular.auth:token';

    this.info = {};

    /**
     * @returns {{user: {name: string}, roles: Array}}
     */
    this.getInfo = function() {
      return this.info;
    };

    this.clear = function() {
      delete this.info.token;
      delete this.info.user;
      delete this.info.expires;

      webStorage.session.remove(TOKEN_STORE_KEY);

      $rootScope.$broadcast('cz.angular.auth:changedState');
    };

    /**
     * @param token
     */
    this.setToken = function(token) {
      if (!token) {
        return;
      }

      webStorage.session.set('cz.angular.auth:token', token);

      this.info.token = token;
      this.info.user = jwtHelper.decodeToken(token);
      this.info.expires = jwtHelper.getTokenExpirationDate(token);

      $rootScope.$broadcast('cz.angular.auth:changedState');
    };

    this.setToken(webStorage.session.get(TOKEN_STORE_KEY));
  };

  angular.module('cz.angular.auth.service', ['angular-jwt', 'webStorageModule'])
      .provider('authServiceSettings', AuthServiceSettingsProvider)
      .factory('authService', authServiceFactory)
      .service('authTokenStore', AuthTokenStore);

})();