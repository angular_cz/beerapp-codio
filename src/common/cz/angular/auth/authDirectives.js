(function() {
  'use strict';

  angular.module('cz.angular.auth')
      .directive('authUserName', function(authService) {
        return {
          scope: {},
          restrict: 'A',
          link: function(scope) {
            scope.auth = authService;
          },
          template: '{{auth.info.user.username}}'
        };
      })

      .directive('authLogoutLink', function(authService) {
        return {
          restrict: 'A',
          link: function(scope, element) {
            element.bind('click', authService.logout.bind(authService));
          }
        };
      })

      .directive('authLoginLink', function(authLoginModal) {
        return {
          restrict: 'A',
          link: function(scope, element) {
            element.bind('click', authLoginModal.prepareLoginModal.bind(authLoginModal));
          }
        };
      })

      .directive('authIsAuthenticated', function(authService) {
        return {
          restrict: 'A',
          link: function(scope, element) {
            element.toggleClass('ng-hide', !authService.isAuthenticated());

            scope.$on('cz.angular.auth:changedState', function() {
              element.toggleClass('ng-hide', !authService.isAuthenticated());
            });
          }
        };
      })

      .directive('authIsNotAuthenticated', function(authService) {
        return {
          scope: {},
          restrict: 'A',
          link: function(scope, element) {
            element.toggleClass('ng-hide', authService.isAuthenticated());

            scope.$on('cz.angular.auth:changedState', function() {
              element.toggleClass('ng-hide', authService.isAuthenticated());
            });
          }
        };
      })

      .directive('authHasRole', function(authService) {
        return {
          scope: {
            role: '@authHasRole'
          },
          restrict: 'A',
          link: function(scope, element) {
            scope.actualize = function() {
              var hasRole = authService.hasRole(scope.role);
              element.toggleClass('ng-hide', !hasRole);
            };

            scope.$on('cz.angular.auth:changedState', function() {
              scope.actualize();
            });

            scope.auth = authService;
            scope.actualize();
          }
        };
      });

})();