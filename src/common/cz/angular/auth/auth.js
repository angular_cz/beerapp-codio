(function() {
  'use strict';

  function loginInterceptor($injector) {

    return {
      responseError: function(response) {
        return $injector.invoke(function(authLoginModal, authServiceSettings, $http, $q) {
          if (response.status === 401) {

            if (response.config.url === authServiceSettings.urls.login) {
              return $q.reject(response);
            }

            return authLoginModal.prepareLoginModal()
                .then(function() {
                  return $http(response.config);
                });

          } else if (response.status === 403) {
            return authLoginModal.prepareRejectModal();
          }

          return $q.reject(response);
        });
      }
    };
  }

  function tokenInterceptor(authTokenStore, authServiceSettings) {
    return {
      request: function(config) {
        config.headers = config.headers || {};
        if (authTokenStore.info.token) {
          config.headers[authServiceSettings.tokenHeaderName] = authTokenStore.info.token;
        }
        return config;
      }
    };
  }

  function registerStateChangeStartListener($rootScope, $state, authService, authLoginModal, $timeout) {

    function hasPermissionForState(state) {
      if (state.data.authRoles) {
        if (!authService.hasSomeRole(state.data.authRoles)) {
          return false;
        }
      }

      return true;
    }

    $rootScope.$on('$stateChangeStart', function stateChangeStartListener(event, toState, toParams) {
      if (!toState.hasOwnProperty('data')) {  // má stav definovanou položku data
        return;
      }

      var mustBeLogged = toState.data.authLogged || toState.data.authRoles;
      if (!mustBeLogged) {  // nemusím být přihlášený
        return;
      }

      if (authService.isAuthenticated()) {
        if (!hasPermissionForState(toState)) {
          event.preventDefault(); // zastavit routing
          $rootScope.$broadcast('cz.angular.auth:permissionError', 'User has not permissions to ' + toState.name);
        }
      } else {
        event.preventDefault(); // zastavit routing
        authLoginModal.prepareLoginModal().then(function() {
              if (!hasPermissionForState(toState)) {
                $rootScope.$broadcast('cz.angular.auth:permissionError', 'User has not permissions to ' + toState.name);
              } else {
                $state.go(toState, toParams);
              }
            },
            function() {
              console.log($state.current);
            });

      }
    });
  }

  angular.module('cz.angular.auth',
      ['ui.router', 'ui.bootstrap', 'cz.angular.auth.service'])
      .config(function($httpProvider) {
        $httpProvider.interceptors.push(loginInterceptor);
        $httpProvider.interceptors.push(tokenInterceptor);
      })
      .run(registerStateChangeStartListener);

})();