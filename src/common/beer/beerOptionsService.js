(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.common.beer.beerOptionsService', [])
      .service('beerOptionsService', function($http, $q, API_URI) {

        var storedOptions;

        this.getOptions = function() {
          if (!storedOptions) {
            return this.readOptions();
          }

          return $q.when(storedOptions);
        };

        this.readOptions = function() {
          return $http.get(API_URI + '/beer/options')
              .then(function(response) {
                storedOptions = response.data;
                return response.data;
              });
        };
      });

})();