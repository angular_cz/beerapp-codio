# frontendová část aplikace https://bitbucket.org/angular_cz/beerapp #



### Instalace ###

naklonujte si repozitář a v root složce projektu spusťte

```
npm install
```

pro vývoj pak
```
gulp devel
```

### Gulp tasks ###

**devel**

* spustí aplikaci ve vývojovém módu (server, livereload, watching)

**build**

* zbuilduje aplikaci do složky build

Testing

```
# karma
npm run test

# protractor

npm run update-webdriver
# pouze před prvním spuštěním protraktoru

npm run protractor
```