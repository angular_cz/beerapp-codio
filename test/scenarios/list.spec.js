'use strict';

var ListPage = require ("./list.po.js");

describe('On beer list', function() {

  beforeEach(function() {

    browser.get('index.html');

    this.listPage = new ListPage();
  });

  describe('when searching for query=Rebel and degree=11', function() {

    beforeEach(function() {
      this.listPage.setFilterQuery('Rebel');
      this.listPage.setFilterDegree(11);

      this.firstRow = this.listPage.getRow(0);
    });

    it('first row should be Rebel', function() {
      expect(this.firstRow.getName()).toBe("Rebel");
    });

    describe('and clicking on first row detail', function() {
      beforeEach(function() {
        this.detailPage = this.firstRow.goToDetail();
      });

      it('name should be Rebel', function() {
        var beerName = this.detailPage.getBeerName();
        expect(beerName).toBe("Rebel");
      });
    });
  });
});
