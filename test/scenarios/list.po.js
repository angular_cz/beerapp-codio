var DetailPage = require('./detail.po.js');

function ListPage() {
  this.setFilterQuery = function(query) {
    element(by.model('filterController.filter.query')).sendKeys(query);
    browser.waitForAngular();
  };

  this.setFilterDegree = function(degree) {
    element(by.model('filterController.filter.degree')).element(by.cssContainingText('option', degree + "")).click();
    browser.waitForAngular();
  };

  this.getRow = function(index) {
    var row = element.all(by.repeater('beer in list.beers')).get(index);

    return new RowElement(row);
  };

  function RowElement(row) {
    this.row = row;
    this.getName = function() {
      return this.row.element(by.binding('beer.name')).getText();
    };

    this.goToDetail = function() {
      row.element(by.css('.detail')).click();

      return new DetailPage();
    };
  }
}

module.exports = ListPage;

